package com.tagelectron.engine.entities;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Renderable;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;

public abstract class Moveable extends Renderable {

    protected Vector2f velocity = new Vector2f(0, 0);

    protected boolean isMovingDown = false;

    protected boolean isMovingUp = false;

    protected boolean isMovingRight = false;

    protected boolean isMovingLeft = false;

    protected boolean isFacingDown = false;

    protected boolean isFacingUp = false;

    protected boolean isFacingLeft = false;

    protected boolean isFacingRight = false;

    protected boolean isMoving = false;



    public Moveable(Texture texture, Vector2f position, Shader shader, Vector2f scale) {
        super(texture, position, shader, scale);
    }

    public Moveable(Vector2f position, Shader shader, Vector2f scale) {
        super(position, shader, scale);
    }

    public Moveable(Texture texture, Vector2f position, Shader shader, Vector2f scale, Model model) {
        super(texture, position, shader, scale, model);
    }

    public Moveable(Vector2f position, Shader shader, Vector2f scale, Model model) {
        super(position, shader, scale, model);
    }

    public void updateVelocity() {
        if (velocity.y > 0) {
            isMovingUp = true;
            isMovingDown = false;
            isFacingRight = false;
            isFacingLeft = false;
            isFacingUp = true;
            isFacingDown = false;
        } else if (velocity.y < 0) {
            isMovingDown = true;
            isMovingUp = false;
            isFacingRight = false;
            isFacingLeft = false;
            isFacingUp = false;
            isFacingDown = true;
        } else {
            isMovingDown = false;
            isMovingUp = false;
        }

        if (velocity.x > 0) {
            isMovingRight = true;
            isMovingLeft = false;
            isFacingRight = true;
            isFacingLeft = false;
            isFacingUp = false;
            isFacingDown = false;
        } else if (velocity.x < 0) {
            isMovingLeft = true;
            isMovingRight = false;
            isFacingRight = false;
            isFacingLeft = true;
            isFacingUp = false;
            isFacingDown = false;
        } else {
            isMovingLeft = false;
            isMovingRight = false;
        }

        isMoving = !(velocity.x < 8E-3 && velocity.x > -8E-3) || !(velocity.y < 8E-3 && velocity.y > -8E-3);

        position.add(velocity, position);
    }

    public Vector2f getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2f velocity) {
        this.velocity = velocity;
    }

    public boolean isMovingDown() {
        return isMovingDown;
    }

    public void setMovingDown(boolean movingDown) {
        this.isMovingDown = movingDown;
    }

    public boolean isMovingRight() {
        return isMovingRight;
    }

    public void setMovingRight(boolean movingRight) {
        isMovingRight = movingRight;
    }

    public boolean isMovingLeft() {
        return isMovingLeft;
    }

    public void setMovingLeft(boolean movingLeft) {
        isMovingLeft = movingLeft;
    }

    public boolean isMovingUp() {
        return isMovingUp;
    }

    public void setMovingUp(boolean movingUp) {
        isMovingUp = movingUp;
    }

    public boolean isFacingDown() {
        return isFacingDown;
    }

    public void setFacingDown(boolean facingDown) {
        isFacingDown = facingDown;
    }

    public boolean isFacingUp() {
        return isFacingUp;
    }

    public void setFacingUp(boolean facingUp) {
        isFacingUp = facingUp;
    }

    public boolean isFacingLeft() {
        return isFacingLeft;
    }

    public void setFacingLeft(boolean facingLeft) {
        isFacingLeft = facingLeft;
    }

    public boolean isFacingRight() {
        return isFacingRight;
    }

    public void setFacingRight(boolean facingRight) {
        isFacingRight = facingRight;
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setMoving(boolean moving) {
        isMoving = moving;
    }
}
