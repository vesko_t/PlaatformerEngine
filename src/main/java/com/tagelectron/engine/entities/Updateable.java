package com.tagelectron.engine.entities;

import org.joml.Vector2f;

/**
 * Created by Vesko on 5.10.2018 г..
 */
public interface Updateable {

    void update(float delta);

    Vector2f getPosition();

    boolean isToBeRemoved();

    void setToBeRemoved(boolean toBeRemoved);

    void constantUpdate(float delta);
}
