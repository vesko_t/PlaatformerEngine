package com.tagelectron.engine.entities;

import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.graphics.particles.ParticleEmitter;
import com.tagelectron.engine.io.Input;
import com.tagelectron.engine.rendering.Camera;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class Updater implements Runnable, EventHandler {

    private int TPS;

    /*private double updateTicks;

    private double deltaTicks = 0;

    private double lastTime;

    private int ticks = 0;

    private long timer;*/

    //private int lastTicks;

    // private boolean running = true;

    private static long totalTicks = 0;

    private static List<Updateable> updateables = new ArrayList<Updateable>();

    private static List<ParticleEmitter> particleEmitters = new ArrayList<>();

    private static List<Updateable> inView = new ArrayList<Updateable>();

    protected static boolean paused = false;

    private float delta;

    public Updater() {
        this.TPS = 60;
        /*this.lastTicks = 60;
        updateTicks = 1000000000 / (double) TPS;*/
        EventManager.getEventHandlers().add(this);
    }

    public Updater(int updatesPerSecond) {
        this.TPS = updatesPerSecond;
       /* this.lastTicks = updatesPerSecond;
        updateTicks = 1000000000 / (double) TPS;*/
        EventManager.getEventHandlers().add(this);
    }

    public void run() {
        /*long now = System.nanoTime();
        deltaTicks += (now - lastTime) / updateTicks;
        lastTime = now;
        if (deltaTicks >= 1) {*/
        updateInView();
        if (!paused) {
            List<Updateable> toBeRemoved = new ArrayList<>();
            for (int i = 0; i < updateables.size(); i++) {
                Updateable updateable = updateables.get(i);
                updateable.constantUpdate(1 / delta);
            }
            for (int i = 0; i < inView.size(); i++) {
                Updateable updateable = inView.get(i);
                updateable.update(1.0f / delta);
                if (updateable.isToBeRemoved()) {
                    toBeRemoved.add(updateable);
                }
            }
            updateables.removeAll(toBeRemoved);
            List<ParticleEmitter> removed = new ArrayList<>();
            for (int i = 0; i < particleEmitters.size(); i++) {
                ParticleEmitter particleEmitter = particleEmitters.get(i);
                particleEmitter.update(1.0f / delta);
                if (particleEmitter.isToBeRemoved()) {
                    removed.add(particleEmitter);
                }
            }
            particleEmitters.removeAll(removed);
        }
        if (GuiManager.getGuiElements() != null && GuiManager.getGuiElements().size() > 0) {
            GuiManager.update();
        }
        update(1 / delta);
        //ticks++;
        totalTicks++;
        //deltaTicks--;
        Input.update();
        /*}
        if (System.currentTimeMillis() - timer > 1000) {
            timer += 1000;
            //System.out.println("Updates : " + ticks);
            lastTicks = ticks;
            ticks = 0;
        }*/
    }

    public abstract void update(float delta);

    private void updateInView() {
        inView = new ArrayList<Updateable>();
        inView.addAll(updateables.stream().filter(Camera::isInView).collect(Collectors.toList()));
    }

    public static List<Updateable> getInView() {
        return inView;
    }

    public void stop() {
        //running = false;
    }

    public static List<Updateable> getUpdateables() {
        return updateables;
    }

    public static void setUpdateables(List<Updateable> updateables) {
        Updater.updateables = updateables;
    }

    public static boolean isPaused() {
        return paused;
    }

    public static void setPaused(boolean paused) {
        Updater.paused = paused;
    }

    public static long getTotalTicks() {
        return totalTicks;
    }

    public static List<ParticleEmitter> getParticleEmitters() {
        return particleEmitters;
    }

    public float getDelta() {
        return delta;
    }

    public void setDelta(float delta) {
        this.delta = delta;
    }
}