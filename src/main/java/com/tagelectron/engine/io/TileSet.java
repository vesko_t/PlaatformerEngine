package com.tagelectron.engine.io;

import com.tagelectron.engine.graphics.Texture;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vesko on 9.10.2018 г..
 */
public class TileSet {

    private Texture tileSet;
    private int width;
    private int height;
    private int tileHeight;
    private int tileWidth;
    private int tileCount;
    private int columns;

    private List<Integer> textureBuffers = new ArrayList<Integer>();

    public Texture getTileSet() {
        return tileSet;
    }

    public void setTileSet(Texture tileSet) {
        this.tileSet = tileSet;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getTileHeight() {
        return tileHeight;
    }

    public void setTileHeight(int tileHeight) {
        this.tileHeight = tileHeight;
    }

    public int getTileWidth() {
        return tileWidth;
    }

    public void setTileWidth(int tileWidth) {
        this.tileWidth = tileWidth;
    }

    public List<Integer> getTextureBuffers() {
        return textureBuffers;
    }


}
