package com.tagelectron.engine.io;

import com.tagelectron.engine.graphics.Texture;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.glfw.GLFWScrollCallback;
import org.lwjgl.glfw.GLFWVidMode;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL.createCapabilities;
import static org.lwjgl.opengl.GL11.*;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Window {

    static long window;
    private static int height, width;
    private boolean fullScreen;
    private Input input;

    public Window(int width, int height, String title, boolean fullScreen, boolean vsync) {
        this.fullScreen = fullScreen;
        Window.height = height;
        Window.width = width;

        if (!glfwInit()) {
            System.err.println("Failed to initialize GLFW");
            System.exit(1);
        }
        GLFWVidMode vidMode = glfwGetVideoMode(glfwGetPrimaryMonitor());

        window = glfwCreateWindow(fullScreen ? vidMode.width() : width, fullScreen ? vidMode.height() : height, title, fullScreen ? glfwGetPrimaryMonitor() : 0, 0);
        //glfwSetWindowSizeCallback(window, new WindowCallback(window));


        if (!fullScreen) {
            glfwSetWindowPos(window, (vidMode.width() - width) / 2, (vidMode.height() - height) / 2);
        } else {
            this.width = vidMode.width();
            this.height = vidMode.height();
        }

        glfwMakeContextCurrent(window);

        createCapabilities();

        input = new Input();
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        if (vsync)
            glfwSwapInterval(1);
    }

    public void prepare() {
        glfwPollEvents();
    }

    public void clear() {
        glClear(GL_COLOR_BUFFER_BIT);
        glClear(GL_DEPTH_BUFFER_BIT);
    }

    public void swapBuffers() {
        glfwSwapBuffers(window);
    }

    public static void close() {
        glfwSetWindowShouldClose(window, true);
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(window);
    }

    public static int getHeight() {
        return height;
    }

    public static void setHeight(int height) {
        Window.height = height;
    }

    public static void setWidth(int width) {
        Window.width = width;
    }

    public static int getWidth() {
        return width;
    }

    public void terminate() {
        glfwTerminate();
    }

    public static void setCursor(Texture texture) {

        GLFWImage image = GLFWImage.malloc().set(texture.getWidth(), texture.getHeight(), texture.getBuffer());

        long cursor = glfwCreateCursor(image, 0, 0);

        glfwSetCursor(window, cursor);
    }

    public static void setScrollCallback(GLFWScrollCallback callback) {
        glfwSetScrollCallback(window, callback);
    }

    public static void setIcon(Texture texture) {
        try (GLFWImage.Buffer images = GLFWImage.malloc(1)) {
            images
                    .position(0)
                    .width(texture.getWidth())
                    .height(texture.getHeight())
                    .pixels(texture.getBuffer());
            glfwSetWindowIcon(window, images);
        }
    }
}

