package com.tagelectron.engine.io;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.world.*;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by vesko on 29.01.18.
 */
public final class MapReader {

    private MapReader() {
    }

    public static TiledMap loadMap(Element element, Vector3f expectedScale) {
        TiledMap map;
        try {
            map = new TiledMap();
            map.setMapDocument(element);
            float tileWidth = Float.parseFloat(element.getAttribute("tilewidth"));
            float tileHeight = Float.parseFloat(element.getAttribute("tileheight"));

            Vector3f scale = new Vector3f(expectedScale.x / (tileWidth / 2.0f), expectedScale.y / (tileHeight / 2.0f), 0);

            Vector2f tileScale = new Vector2f((tileWidth / 2.0f) * scale.x, (tileHeight / 2.0f) * scale.y);

            map.setWidth(Integer.parseInt(element.getAttribute("width")));
            map.setHeight(Integer.parseInt(element.getAttribute("height")));
            map.setTileScale(tileScale);

            Map<Integer, MapObject> objectList = new HashMap<>();
            Map<String, List<MapObject>> mapObjectsWithNames = new HashMap<>();
            Map<String, List<MapObject>> mapObjectsWithTypes = new HashMap<>();


            Element properties = ((Element) element.getElementsByTagName("properties").item(0));
            if (properties != null) {
                NodeList propertyList = properties.getElementsByTagName("property");
                for (int i = 0; i < propertyList.getLength(); i++) {
                    Element property = (Element) propertyList.item(i);
                    if (property.getAttribute("name").equals("name")) {
                        map.setName(property.getAttribute("value"));
                    } else if (property.getAttribute("name").equals("type")) {
                        map.setType(property.getAttribute("value"));
                    }
                }
            }
            NodeList objectGroup = element.getElementsByTagName("objectgroup");
            if (objectGroup.getLength() != 0) {
                for (int j = 0; j < objectGroup.getLength(); j++) {
                    NodeList objects = ((Element) objectGroup.item(j)).getElementsByTagName("object");
                    for (int i = 0; i < objects.getLength(); i++) {
                        Element object = (Element) objects.item(i);
                        String objectName = object.getAttribute("name");
                        int objectId = Integer.parseInt(object.getAttribute("id"));
                        Vector2f objectPosition = new Vector2f(Float.parseFloat(object.getAttribute("x")) * scale.x, (map.getHeight() * (tileScale.y * 2)) - Float.parseFloat(object.getAttribute("y")) * scale.y);
                        String widthAttrib = object.getAttribute("width");
                        String heightAttrib = object.getAttribute("height");
                        String objectType = object.getAttribute("type");

                        MapObject mapObject = new MapObject(objectPosition, objectId);
                        if (!widthAttrib.equals("") && !heightAttrib.equals("")) {
                            Vector2f objectScale = new Vector2f((Float.parseFloat(widthAttrib) / 2.0f) * scale.x, (Float.parseFloat(heightAttrib) / 2.0f) * scale.y);
                            mapObject.setScale(objectScale);
                            mapObject.setPosition(new Vector2f(objectPosition.x - objectScale.x, objectPosition.y - objectScale.y));
                        }
                        if (!objectName.equals("")) {
                            mapObject.setName(objectName);
                        }
                        if (!objectType.equals("")) {
                            mapObject.setType(objectType);
                        }

                        objectList.put(objectId, mapObject);
                        if (mapObject.getName() != null) {
                            if (mapObjectsWithNames.containsKey(mapObject.getName())) {
                                mapObjectsWithNames.get(mapObject.getName()).add(mapObject);
                            } else {
                                List<MapObject> mObj = new ArrayList<>();
                                mObj.add(mapObject);
                                mapObjectsWithNames.put(mapObject.getName(), mObj);
                            }
                        }

                        if (mapObject.getType() != null) {
                            if (mapObjectsWithTypes.containsKey(mapObject.getType())) {
                                mapObjectsWithTypes.get(mapObject.getType()).add(mapObject);
                            } else {
                                List<MapObject> mObj = new ArrayList<>();
                                mObj.add(mapObject);
                                mapObjectsWithTypes.put(mapObject.getType(), mObj);
                            }
                        }

                        Element objectProperties = (Element) object.getElementsByTagName("properties").item(0);

                        if (objectProperties != null) {
                            for (int k = 0; k < objectProperties.getElementsByTagName("property").getLength(); k++) {
                                Element property = (Element) objectProperties.getElementsByTagName("property").item(k);
                                mapObject.getProperties().put(property.getAttribute("name"), property.getAttribute("value"));
                            }
                        }

                        objectList.put(objectId, new MapObject(objectName.equals("") ? null : objectName, objectPosition, objectId));
                    }
                }
            }

            NodeList layers = element.getElementsByTagName("layer");
            Map<String, Layer> layersMap = new HashMap<String, Layer>();
            List<String> layerKeys = new ArrayList<>();
            for (int i = 0; i < layers.getLength(); i++) {
                Element layer = (Element) layers.item(i);
                Element data = (Element) layer.getElementsByTagName("data").item(0);

                Layer layer1 = new Layer(map);
                String layerName = layer.getAttribute("name");
                layersMap.put(layerName, layer1);
                layerKeys.add(layerName);
                String coords = data.getTextContent();

                List<String> lines = Arrays.asList(coords.split("\n"));

                Collections.reverse(lines);

                for (int i1 = 0; i1 < lines.size(); i1++) {
                    String line = lines.get(i1);
                    String coord[] = line.split(",");
                    List<Tile> row = new ArrayList<Tile>();
                    for (int j = 0; j < coord.length; j++) {
                        String number = coord[j];
                        if (!number.equals("")) {
                            if (Integer.parseInt(coord[j]) != 0) {
                                row.add(new TexturedTile(j, i1, tileScale, Integer.parseInt(coord[j])));
                            } else {
                                row.add(new Tile(j, i1, tileScale));
                            }
                        }
                    }
                    layer1.getRows().add(row);
                }
            }
            map.setTileSet(loadSingleTileSet(map.getName()));
            map.setLayers(layersMap);
            map.setLayerNames(layerKeys);
            map.setMapObjects(mapObjectsWithNames);
            map.setObjectList(objectList);
            map.setMapObjectsTypes(mapObjectsWithTypes);
        } catch (Exception e) {
            e.printStackTrace();
            map = null;
        }
        return map;
    }

    /**
     * Loads .tmx file containing map information
     *
     * @param mapName path to map
     * @return returns map object
     */
    public static TiledMap loadMap(String mapName, Vector3f expectedScale) {
        InputStream is = MapReader.class.getResourceAsStream("/maps/" + mapName + "/map.tmx");
        TiledMap map;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(is);
            map = loadMap(document.getDocumentElement(), expectedScale);
        } catch (Exception e) {
            e.printStackTrace();
            map = null;
        }
        return map;
    }

    /**
     * loads tile set
     *
     * @param filename file path to ts
     * @return tileset object
     *//*
    public static TileSet loadTileSet(String filename) {
        InputStream is = Class.class.getResourceAsStream("/maps/" + filename + "/tileSet.tsx");
        TileSet set;
        try {
            set = new TileSet();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(is);
            Element element = document.getDocumentElement();
            int length = Integer.parseInt(element.getAttribute("tilecount"));
            for (int i = 0; i < length; i++) {
                Element tile = (Element) document.getElementsByTagName("image").item(i);
                String source = tile.getAttribute("source");
                Vector3f scale = new Vector3f(Float.parseFloat(tile.getAttribute("width")), Float.parseFloat(tile.getAttribute("height")), 0);
                set.setTexture(i, new Texture("/maps/" + filename + "/" + source));
                set.setScale(i, scale);
            }
        } catch (Exception e) {
            e.printStackTrace();
            set = null;
        }
        return set;
    }*/
    public static TileSet loadSingleTileSet(String filename) {
        TileSet tileSet = null;

        InputStream is = MapReader.class.getResourceAsStream("/maps/" + filename + "/tileSet.tsx");
        try {
            tileSet = new TileSet();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setValidating(false);

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(is);
            Element element = (Element) document.getElementsByTagName("tileset").item(0);
            Element image = (Element) element.getElementsByTagName("image").item(0);
            float tileWidth = Float.parseFloat(element.getAttribute("tilewidth"));
            float tileHeight = Float.parseFloat(element.getAttribute("tileheight"));
            int tileCount = Integer.parseInt(element.getAttribute("tilecount"));
            int columns = Integer.parseInt(element.getAttribute("columns"));
            int width = Integer.parseInt(image.getAttribute("width"));
            int height = Integer.parseInt(image.getAttribute("height"));

            Texture texture = new Texture("/maps/" + filename + "/" + image.getAttribute("source"));

            tileSet.setTileSet(texture);

            float glWidth = tileWidth / width;
            float glHeight = tileHeight / height;

            int rows = tileCount / columns;

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    float[] textures = {
                            j * glWidth, i * glHeight,
                            j * glWidth + glWidth, i * glHeight,
                            j * glWidth + glWidth, i * glHeight + glHeight,
                            j * glWidth, i * glHeight + glHeight
                    };
                    tileSet.getTextureBuffers().add(Model.createVbo(textures));
                }
            }

            return tileSet;
        } catch (IOException exception) {
            exception.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return tileSet;
    }

}
