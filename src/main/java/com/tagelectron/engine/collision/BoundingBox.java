package com.tagelectron.engine.collision;

import org.joml.Vector2f;
import org.lwjgl.system.CallbackI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 26.01.18.
 */
public class BoundingBox {
    private Vector2f position;
    private Vector2f scale;
    private Vector2f offset;

    public BoundingBox(Vector2f position, Vector2f scale) {
        this(position, scale, new Vector2f());
    }

    public BoundingBox(Vector2f position, Vector2f scale, Vector2f offset) {
        this.position = position;
        this.scale = scale;
        this.offset = offset;
    }

    public boolean isColliding(BoundingBox box) {
        Vector2f position = getPosition();
        return position.x - scale.x < box.getPosition().x + box.getScale().x &&
                position.x + scale.x > box.getPosition().x - box.getScale().x &&
                position.y - scale.y < box.getPosition().y + box.getScale().y &&
                position.y + scale.y > box.getPosition().y - box.getScale().y;
    }

    public boolean isInBoundingBox(Vector2f vector2f) {
        Vector2f position = getPosition();
        return vector2f.x >= position.x - scale.x && vector2f.x <= position.x + scale.x &&
                vector2f.y >= position.y - scale.y && vector2f.y <= position.y + scale.y;
    }

    public boolean isInBoundingBox(Vector2f vector2f, BoundingBox boundingBox) {
        return vector2f.x >= boundingBox.getPosition().x - boundingBox.getScale().x && vector2f.x <= boundingBox.getPosition().x + boundingBox.getScale().x &&
                vector2f.y >= boundingBox.getPosition().y - boundingBox.getScale().y && vector2f.y <= boundingBox.getPosition().y + boundingBox.getScale().y;
    }

    public Vector2f leftBottomCorner() {
        Vector2f position = getPosition();
        return new Vector2f(position.x - scale.x(), position.y - scale.y);
    }

    public Vector2f rightBottomCorner() {
        Vector2f position = getPosition();
        return new Vector2f(position.x + scale.x(), position.y - scale.y);
    }

    public Vector2f leftTopCorner() {
        Vector2f position = getPosition();
        return new Vector2f(position.x - scale.x(), position.y + scale.y);
    }

    public Vector2f rightTopCorner() {
        Vector2f position = getPosition();
        return new Vector2f(position.x + scale.x(), position.y + scale.y);
    }


    protected boolean isLeft(BoundingBox boundingBox) {
        Vector2f position = getPosition();
        return position.x + scale.x > boundingBox.getPosition().x - boundingBox.getScale().x;
    }

    protected boolean isRight(BoundingBox boundingBox) {
        Vector2f position = getPosition();
        return position.x - scale.x < boundingBox.getPosition().x + boundingBox.getScale().x;
    }

    protected boolean isTop(BoundingBox boundingBox) {
        Vector2f position = getPosition();
        return position.y - scale.y < boundingBox.getPosition().y + boundingBox.getScale().y;
    }

    protected boolean isBottom(BoundingBox boundingBox) {
        Vector2f position = getPosition();
        return position.y + scale.y > boundingBox.getPosition().y - boundingBox.getScale().y;
    }

    public Vector2f getPosition() {
        return position.add(offset, new Vector2f());
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public Vector2f getScale() {
        return scale;
    }

    public void setScale(Vector2f scale) {
        this.scale = scale;
    }

    public float getHeight() {
        return scale.y() * 2;
    }

    public float getWidth() {
        return scale.x() * 2;
    }

    public void addPosition(Vector2f vector3f) {
        position.add(vector3f);
    }

    public Vector2f getOffset() {
        return offset;
    }

    public void setOffset(Vector2f offset) {
        this.offset = offset;
    }
}
