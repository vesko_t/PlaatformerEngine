package com.tagelectron.engine.collision;

import com.tagelectron.engine.entities.Moveable;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.world.TexturedTile;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.engine.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.List;

/**
 * Created by vesko on 09.02.18.
 */
public abstract class Collidable extends Moveable {

    protected BoundingBox terrainBox;
    protected BoundingBox entityBox;
    protected String collisionLayerName;
    protected Vector2i collisionRadius;

    public Collidable(Texture texture, Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius) {
        super(texture, position, shader, scale);
        this.collisionLayerName = collisionLayerName;
        this.collisionRadius = collisionRadius;
        terrainBox = new BoundingBox(position, scale);
        entityBox = new BoundingBox(position, scale);
    }

    public Collidable(Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius) {
        super(position, shader, scale);
        this.collisionLayerName = collisionLayerName;
        this.collisionRadius = collisionRadius;
        terrainBox = new BoundingBox(position, scale);
        entityBox = new BoundingBox(position, scale);
    }

    public Collidable(Texture texture, Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, Model model) {
        super(texture, position, shader, scale, model);
        this.collisionLayerName = collisionLayerName;
        this.collisionRadius = collisionRadius;
        terrainBox = new BoundingBox(position, scale);
        entityBox = new BoundingBox(position, scale);
    }

    public Collidable(Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Vector2i collisionRadius, Model model) {
        super(position, shader, scale, model);
        this.collisionLayerName = collisionLayerName;
        this.collisionRadius = collisionRadius;
        terrainBox = new BoundingBox(position, scale);
        entityBox = new BoundingBox(position, scale);
    }

    public void updateCollision() {
        if (Updater.getUpdateables() != null && Updater.getUpdateables().size() > 0) {
            Updater.getInView().stream().filter(updateable -> updateable instanceof Collidable).forEach(updateable -> {
                checkEntityAround((Collidable) updateable);
                if (!updateable.equals(this)) {
                    BoundingBox xBoundingBox = new BoundingBox(new Vector2f(position.x + velocity.x, position.y), entityBox.getScale());
                    BoundingBox yBoundingBox = new BoundingBox(new Vector2f(position.x, position.y + velocity.y), entityBox.getScale());
                    if (xBoundingBox.isColliding(((Collidable) updateable).getEntityBox())) {
                        velocity.x = 0;
                        onCollision((Collidable) updateable, new Collision(Collision.AXIS_X));
                    }
                    if (yBoundingBox.isColliding(((Collidable) updateable).getEntityBox())) {
                        velocity.y = 0;
                        onCollision((Collidable) updateable, new Collision(Collision.AXIS_Y));
                    }
                }
            });
        }

        if (World.getCurrentMap() != null && collisionLayerName != null) {
            List<Tile> inView = World.getCurrentMap().getLayers().get(collisionLayerName).getTilesAround(World.getWorldPosition(position), collisionRadius);
            inView.stream().filter(tile -> tile instanceof TexturedTile).forEach(tile -> {
                BoundingBox xBoundingBox = new BoundingBox(new Vector2f(position.x + velocity.x, position.y), terrainBox.getScale());
                BoundingBox yBoundingBox = new BoundingBox(new Vector2f(position.x, position.y + velocity.y), terrainBox.getScale());
                if (xBoundingBox.isColliding(tile.getBoundingBox())) {
                    velocity.x = 0;
                    onCollision(tile, new Collision(Collision.AXIS_X));
                }
                if (yBoundingBox.isColliding(tile.getBoundingBox())) {
                    velocity.y = 0;
                    onCollision(tile, new Collision(Collision.AXIS_Y));
                }
            });
        }

    }

    protected abstract void checkEntityAround(Collidable collidable);

    protected abstract void onCollision(Tile tile, Collision collision);

    protected abstract void onCollision(Collidable collidable, Collision collision);

    @Override
    public void updateVelocity() {
        super.updateVelocity();
        terrainBox.setPosition(position);
        entityBox.setPosition(position);
    }

    public boolean isMovingDown() {
        return isMovingDown;
    }

    public void setMovingDown(boolean movingDown) {
        this.isMovingDown = movingDown;
    }

    public Vector2f getPosition() {
        return position;
    }

    public Vector2f getScale() {
        return scale;
    }

    public BoundingBox getTerrainBox() {
        return terrainBox;
    }

    public void setTerrainBox(BoundingBox terrainBox) {
        this.terrainBox = terrainBox;
    }

    public BoundingBox getEntityBox() {
        return entityBox;
    }

    public void setEntityBox(BoundingBox entityBox) {
        this.entityBox = entityBox;
    }
}
