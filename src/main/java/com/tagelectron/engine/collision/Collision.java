package com.tagelectron.engine.collision;

public class Collision {

    public static int AXIS_X = 0;

    public static int AXIS_Y = 1;

    private int collisionAxis;

    Collision(int collisionAxis){
        this.collisionAxis = collisionAxis;
    }

    public int getCollisionAxis() {
        return collisionAxis;
    }

    void setCollisionAxis(int collisionAxis) {
        this.collisionAxis = collisionAxis;
    }
}
