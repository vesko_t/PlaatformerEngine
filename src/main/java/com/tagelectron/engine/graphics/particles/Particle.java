package com.tagelectron.engine.graphics.particles;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Created by vesko on 13.11.2018 г..
 */
public class Particle {
    private Matrix4f matrix = new Matrix4f().identity();
    private Vector2f velocity;
    private int aliveTime = 1;
    private int maxLife;
    private Vector2f position;

    private boolean dead;

    public Particle(Vector2f velocity, int maxLife, Vector2f scale, Vector2f position) {
        this.velocity = velocity;
        this.maxLife = maxLife;
        this.position = new Vector2f(position);
        matrix.scale(new Vector3f(scale, 0));
    }

    public void update() {
        if (aliveTime == maxLife) {
            dead = true;
            return;
        }
        position.add(velocity);
        matrix.setTranslation(new Vector3f(position, 0));
        aliveTime++;
    }

    public Matrix4f getMatrix() {
        return matrix;
    }

    public int getAliveTime() {
        return aliveTime;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public boolean isDead() {
        return dead;
    }
}
