package com.tagelectron.engine.graphics.particles;

import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by vesko on 13.11.2018 г..
 */
public class ParticleEmitter {

    private static List<Particle> particles = new ArrayList<>();
    private static final Model model = new Model();

    private static final Random random = new Random();
    private Texture texture;
    private Vector2f position;
    private Shader shader;
    private Vector2f scale;
    private Vector2f particleVelocity;
    private Vector2f xRange;
    private Vector2f yRange;
    private int particleTicks;
    private int particleLife;
    private boolean toBeRemoved = false;

    public ParticleEmitter(ParticleEmitter particleEmitter) {
        texture = particleEmitter.texture;
        position = new Vector2f(particleEmitter.position);
        shader = particleEmitter.shader;
        scale = particleEmitter.scale;
        particleVelocity = new Vector2f(particleEmitter.particleVelocity);
        xRange = new Vector2f(particleEmitter.xRange);
        yRange = new Vector2f(particleEmitter.yRange);
        particleTicks = particleEmitter.particleTicks;
        particleLife = particleEmitter.particleLife;
        toBeRemoved = particleEmitter.toBeRemoved;
    }

    public ParticleEmitter(Texture texture, Vector2f position, Shader shader, Vector2f scale, Vector2f particleVelocity, int particleLife, int particleTicks, Vector2f xRange, Vector2f yRange) {
        this.texture = texture;
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        this.particleVelocity = particleVelocity;
        this.particleLife = particleLife;
        this.particleTicks = particleTicks;
        this.xRange = xRange;
        this.yRange = yRange;
    }

    public ParticleEmitter(Texture texture, Vector2f position, Shader shader, Vector2f scale, Vector2f particleVelocity, int particleLife, int particleTicks) {
        this.texture = texture;
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        this.particleVelocity = particleVelocity;
        this.particleLife = particleLife;
        this.particleTicks = particleTicks;
    }

    public void update(float v) {
        if (particleTicks != 0) {
            if (Updater.getTotalTicks() % particleTicks == 0) {
                Vector2f velocity = new Vector2f();
                particleVelocity.mul(v, velocity);
                if (xRange != null) {
                    particleVelocity.x = xRange.x + random.nextFloat() * (xRange.y - xRange.x);
                }
                if (yRange != null) {
                    particleVelocity.y = yRange.x + random.nextFloat() * (yRange.y - yRange.x);
                }
                particles.add(new Particle(velocity, particleLife, scale, position));
            }
        }
        List<Particle> toBeRemoved = new ArrayList<>();
        for (Particle particle : particles) {
            if (particle != null) {
                if (particle.isDead()) {
                    toBeRemoved.add(particle);
                    continue;
                }
                particle.update();
            }
        }
        particles.removeAll(toBeRemoved);
    }

    public void setPosition(Vector2f position) {
        this.position = new Vector2f(position);
    }

    public void remove() {
        toBeRemoved = true;
    }

    public boolean isToBeRemoved() {
        return toBeRemoved;
    }

    public void render() {
        texture.bind(0);
        shader.bind();
        shader.setUniform("projection", Camera.getProjection());
        shader.setUniform("sampler", 0);
        model.bindVertexBuffer();
        model.bindTextureBuffer();
        for (int i = 0; i < particles.size(); i++) {
            Particle particle = particles.get(i);
            if (particle != null) {
                shader.setUniform("model", particle.getMatrix());
                shader.setUniform("alpha", ((float) particle.getAliveTime() / (float) particle.getMaxLife()));
            }
            model.draw();
        }
        model.unbind();
    }

    public Vector2f getParticleVelocity() {
        return particleVelocity;
    }

    public Vector2f getxRange() {
        return xRange;
    }

    public Vector2f getyRange() {
        return yRange;
    }
}
