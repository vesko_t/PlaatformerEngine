package com.tagelectron.engine.graphics;

import com.tagelectron.engine.rendering.Shader;

/**
 * Created by Vesko on 13.11.2018 г..
 */
public class Background {
    private Model model = new Model();
    private Texture texture;
    private Shader shader;

    public Background(Texture texture, Shader shader) {
        this.texture = texture;
        this.shader = shader;
    }

    public void render() {
        shader.bind();
        texture.bind(0);
        model.render();
    }
}
