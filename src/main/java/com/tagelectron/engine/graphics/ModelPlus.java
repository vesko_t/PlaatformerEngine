package com.tagelectron.engine.graphics;

import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.GL_FLOAT;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * Created by Vesko on 12.10.2018 г..
 */
public class ModelPlus {

    private int count;
    private int drawMode;
    private int vboID;

    public ModelPlus(float[] vertices, int drawMode) {
        count = vertices.length / 2;
        this.drawMode = drawMode;


        try (MemoryStack stack = stackPush()) {
            vboID = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, vboID);
            FloatBuffer floatBuffer = stack.mallocFloat(vertices.length);
            floatBuffer.put(vertices).position(0);
            glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
        }
    }

    public void bind() {
        glBindBuffer(GL_ARRAY_BUFFER, vboID);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
    }

    public void draw() {
        glDrawArrays(drawMode, 0, count);
    }

    public void unbind() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDisableVertexAttribArray(0);
    }

    public void render() {
        bind();
        draw();
        unbind();
    }

    public void setVertices(float[] vertices) {
        glBindBuffer(GL_ARRAY_BUFFER, vboID);
        try (MemoryStack stack = stackPush()) {
            FloatBuffer floatBuffer = stack.mallocFloat(vertices.length);
            floatBuffer.put(vertices);
            floatBuffer.flip();
            glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
        }
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
}
