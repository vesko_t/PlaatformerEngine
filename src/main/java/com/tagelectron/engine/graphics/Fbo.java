package com.tagelectron.engine.graphics;

import com.tagelectron.engine.io.Window;
import org.joml.Vector2f;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.*;


/**
 * Created by vesko on 02.03.18.
 */
public class Fbo {
    private int fboId;
    private int textureId;
    private int width;
    private int height;
    private Texture texture;


    public Fbo(int width, int height) {
        this.width = width;
        this.height = height;
        generateFBO();
    }

    private void generateFBO() {
        fboId = glGenFramebuffers();

        textureId = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, textureId);
        glTexParameterIi(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterIi(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, textureId, 0);

        this.texture = new Texture(textureId, width, height);
    }

    public void bind() {
        glBindTexture(GL_TEXTURE_2D, 0);//To make sure the textureId isn't bound
        glBindFramebuffer(GL_FRAMEBUFFER, fboId);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT);
    }

    public void unbind() {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, Window.getWidth(), Window.getHeight());
    }

    public int getTextureId() {
        return textureId;
    }

    public Texture getTexture() {
        return texture;
    }

    public void bindRenderTexture(int sampler) {
        glActiveTexture(GL_TEXTURE0 + sampler);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, textureId);
    }

    public Vector2f getResoulution() {
        return new Vector2f(width, height);
    }
}
