package com.tagelectron.engine.graphics.gui;

public interface GuiListener {

    public void onAction(String command);

}
