package com.tagelectron.engine.graphics.gui;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;

public class GuiScrollPannel extends GuiElement {


    public GuiScrollPannel(Vector2f position, Vector2f scale, boolean visible) {
        super(position, scale, visible);
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {

    }
}
