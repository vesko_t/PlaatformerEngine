package com.tagelectron.engine.graphics.gui;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.text.GLText;
import org.joml.Vector2f;

public class GuiLabel extends GuiElement {
    protected GLText text;

    public GuiLabel(Vector2f position, boolean visible, GLText text) {
        super(position, new Vector2f(32), visible);
        this.text = text;
    }

    @Override
    public void render(Shader textureShader, Shader noTextureShader, Model model) {
        text.render();
    }

    public GLText getText() {
        return text;
    }

    public void setText(GLText text) {
        this.text = text;
    }
}
