package com.tagelectron.engine.graphics;

import org.lwjgl.opengl.GL15;
import org.lwjgl.system.MemoryStack;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_FLOAT;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER;
import static org.lwjgl.opengl.GL15.GL_STATIC_DRAW;
import static org.lwjgl.opengl.GL15.glBindBuffer;
import static org.lwjgl.opengl.GL15.glBufferData;
import static org.lwjgl.opengl.GL15.glGenBuffers;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.system.MemoryStack.stackPush;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Model {
    private int vId, tId, iId, count;

    private static List<Integer> vbos = new ArrayList<>();

    public Model() {
        float[] vertices = new float[]{
                -1f, 1f, //TOP LEFT
                1f, 1f,   //TOP LEFT
                1f, -1f, //BOTTOM RIGHT
                -1f, -1f, //BOTTOM LEFT
        };

        float[] textures = new float[]{
                0, 0,
                1, 0,
                1, 1,
                0, 1
        };

        fillBuffers(vertices, textures);
    }

    public Model(float[] textures) {
        float[] vertices = new float[]{
                -1f, 1f, //TOP LEFT
                1f, 1f,   //TOP LEFT
                1f, -1f, //BOTTOM RIGHT
                -1f, -1f, //BOTTOM LEFT
        };
        fillBuffers(vertices, textures);
    }

    public Model(float[] vertices, float[] texCoords) {
        fillBuffers(vertices, texCoords);
    }


    public void setTextureCoords(float textureCoords[]) {
        tId = Model.createVbo(textureCoords);
    }

    private void fillBuffers(float[] vertices, float[] texCoords) {
        count = vertices.length / 2;

        tId = Model.createVbo(texCoords);

        vId = Model.createVbo(vertices);
    }

    public void bindVertexBuffer() {
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);
    }

    public void bindTextureBuffer() {
        bindTextureBuffer(tId);
    }

    public void bindTextureBuffer(int tId) {
        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);
    }

    public void draw() {
        glDrawArrays(GL_QUADS, 0, count);
    }

    public void unbind() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }

    public void render() {

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glBindBuffer(GL_ARRAY_BUFFER, vId);
        glVertexAttribPointer(0, 2, GL_FLOAT, false, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, tId);
        glVertexAttribPointer(1, 2, GL_FLOAT, false, 0, 0);

        glDrawArrays(GL_QUADS, 0, count);

        glBindBuffer(GL_ARRAY_BUFFER, 0);

        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);
    }

    public static int createVbo(float[] data) {
        int bufferId = 0;
        try (MemoryStack stack = stackPush()) {
            bufferId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, bufferId);
            FloatBuffer floatBuffer = stack.mallocFloat(data.length);
            floatBuffer.put(data);
            floatBuffer.position(0);
            glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW);
            //memFree(floatBuffer);
        }
        vbos.add(bufferId);
        return bufferId;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public int getvId() {
        return vId;
    }

    public void setvId(int vId) {
        this.vId = vId;
    }

    public int getiId() {
        return iId;
    }

    public void setiId(int iId) {
        this.iId = iId;
    }

    public static void cleanUp(){
        vbos.forEach(GL15::glDeleteBuffers);
    }
}
