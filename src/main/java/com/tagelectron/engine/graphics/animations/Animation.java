package com.tagelectron.engine.graphics.animations;

import com.tagelectron.engine.io.Timer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vesko on 23.02.18.
 */
public abstract class Animation {

    protected int pointer;

    protected int timesToRun = -1;

    protected int timesRan = 0;

    protected int framesCount;

    protected boolean running = false;

    protected boolean action = true;

    protected double elapsedTime, currentTime, lastTime, FPS;

    Animation(double FPS, int timesToRun) {
        this(FPS);
        this.timesToRun = timesToRun - 1;
    }

    Animation(double FPS) {
        this.pointer = 0;
        this.elapsedTime = 0;
        this.currentTime = 0;
        this.lastTime = Timer.getTime();
        this.FPS = 1.0 / FPS;
        stop();
    }

    public void start() {
        this.lastTime = Timer.getTime();
        pointer = 0;
        running = true;
        timesRan = 0;
    }

    public void stop() {
        pointer = 0;
        running = false;
        timesRan = 0;
    }

    public boolean isRunning() {
        return running;
    }

    protected abstract void action(int pointer);


    public void render() {
        if (running) {
            this.currentTime = Timer.getTime();
            this.elapsedTime += currentTime - lastTime;

            if (elapsedTime >= FPS) {
                elapsedTime -= FPS;
                pointer++;
            }

            if (pointer >= framesCount) {
                pointer = 0;
                if ((timesToRun > -1)) {
                    timesRan++;
                    if (timesRan >= timesToRun) {
                        running = false;
                    }
                }
            }

            if (action) {
                action(pointer);
            }
            this.lastTime = currentTime;
        }
    }

    public boolean isAction() {
        return action;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public int getPointer() {
        return pointer;
    }

    public void setPointer(int pointer) {
        this.pointer = pointer;
    }

    public int getTimesToRun() {
        return timesToRun + 1;
    }

    public int getFramesCount() {
        return framesCount;
    }

    public double getFPS() {
        return (1 / FPS);
    }
}
