package com.tagelectron.engine.graphics.animations;

import java.util.ArrayList;
import java.util.List;

public class MultipleTextureAnimation extends Animation {

    private List<TextureAnimation> animations = new ArrayList<>();

    public MultipleTextureAnimation(double FPS, int timesToRun, int framesCount) {
        super(FPS, timesToRun);
        this.framesCount = framesCount;
    }

    public MultipleTextureAnimation(double FPS, int framesCount) {
        super(FPS);
        this.framesCount = framesCount;
    }

    public MultipleTextureAnimation(double FPS, int timesToRun, int framesCount, List<TextureAnimation> animations) {
        super(FPS, timesToRun);
        this.animations = animations;
        this.framesCount = framesCount;
    }

    public MultipleTextureAnimation(double FPS, List<TextureAnimation> animations, int framesCount) {
        super(FPS);
        this.animations = animations;
        this.framesCount = framesCount;
    }

    public void addAnimation(TextureAnimation textureAnimation) {
        textureAnimation.setTexturePointer(animations.size());
        animations.add(textureAnimation);
    }

    public List<TextureAnimation> getAnimations() {
        return animations;
    }

    public void setAnimations(List<TextureAnimation> animations) {
        this.animations = animations;
    }

    @Override
    protected void action(int pointer) {
        /*animations.forEach(textureAnimation -> {
            textureAnimation.action(pointer);
        });*/
    }

    @Override
    public void render() {
        super.render();
        animations.forEach(Animation::render);
    }

    @Override
    public void start() {
        super.start();
        if (animations != null) {
            animations.forEach(TextureAnimation::start);
        }
    }

    @Override
    public void stop() {
        super.stop();
        if (animations != null) {
            animations.forEach(TextureAnimation::stop);
        }
    }
}
