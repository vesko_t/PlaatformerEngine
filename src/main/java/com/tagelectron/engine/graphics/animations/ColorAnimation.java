package com.tagelectron.engine.graphics.animations;

import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by vesko on 23.02.18.
 */
public class ColorAnimation extends Animation {

    protected List<Vector4f> frames = new ArrayList<>();

    private Shader shader;

    public ColorAnimation(double FPS, Shader shader, int timesToRun, Vector4f... colors) {
        super(FPS, timesToRun);
        this.shader = shader;
        frames = Arrays.asList(colors);
        framesCount = frames.size();
    }

    public List getFrames() {
        return frames;
    }

    public void addFrame(Vector4f color) {
        frames.add(color);
    }

    protected void action(int pointer) {
        shader.setUniform("color", (Vector4f) frames.get(pointer));
    }
}
