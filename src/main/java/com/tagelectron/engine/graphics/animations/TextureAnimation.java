package com.tagelectron.engine.graphics.animations;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Renderable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by vesko on 19.02.18.
 */
public class TextureAnimation extends Animation {

    protected List<Integer> frames = new ArrayList<>();

    private Texture spriteSheet;

    private Renderable renderable;

    private int texturePointer = 0;

    private int sheetWidth;

    private int sheetHeight;

    public TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps, Renderable renderable) {
        super(fps);
        this.renderable = renderable;
        this.spriteSheet = spriteSheet;
        this.running = true;
        sheetWidth = spriteSheet.getWidth();
        sheetHeight = spriteSheet.getHeight();
        createFrames(cols, rows, totalCols, startingRow);
    }

    public TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps, int startingCol, Renderable renderable) {
        super(fps);
        this.renderable = renderable;
        this.spriteSheet = spriteSheet;
        this.running = true;
        sheetWidth = spriteSheet.getWidth();
        sheetHeight = spriteSheet.getHeight();
        createFrames(startingCol, cols, rows, totalCols, startingRow);
    }

    public TextureAnimation(Texture spriteSheet, int cols, int rows, int totalCols, int startingRow, double fps, Renderable renderable, int timesToRun) {
        super(fps, timesToRun);
        this.renderable = renderable;
        this.spriteSheet = spriteSheet;
        this.running = true;
        sheetWidth = spriteSheet.getWidth();
        sheetHeight = spriteSheet.getHeight();
        createFrames(cols, rows, totalCols, startingRow);
    }

    public TextureAnimation(int fps, int timesToRun, List<Integer> frames, Renderable renderable, Texture spriteSheet) {
        super(fps, timesToRun);
        this.running = true;
        this.frames = frames;
        this.framesCount = frames.size();
        this.spriteSheet = spriteSheet;
        this.renderable = renderable;
    }

    private void createFrames(int cols, int rows, int totalCols, int startingRow) {
        createFrames(0, cols, rows, totalCols, startingRow);
    }

    private void createFrames(int startCol, int cols, int rows, int totalCols, int startingRow) {
        float glWidth = ((float) sheetWidth / (float) totalCols) / (float) sheetWidth;
        float glHeight = ((float) sheetHeight / (float) rows) / (float) sheetHeight;

        for (int i = startCol; i < cols; i++) {
            float[] textureCoordinates = new float[]{
                    i * glWidth, startingRow * glHeight,
                    i * glWidth + glWidth, startingRow * glHeight,
                    i * glWidth + glWidth, startingRow * glHeight + glHeight,
                    i * glWidth, startingRow * glHeight + glHeight
            };
            frames.add(Model.createVbo(textureCoordinates));
        }
        framesCount = frames.size();
    }


    public static List<Integer> createFrames(int cols, int rows, int totalCols, int startingRow, int sheetWidth, int sheetHeight) {
        float glWidth = ((float) sheetWidth / (float) totalCols) / (float) sheetWidth;
        float glHeight = ((float) sheetHeight / (float) rows) / (float) sheetHeight;

        List<Integer> frames = new ArrayList<>();

        for (int i = 0; i < cols; i++) {
            float[] textureCoordinates = new float[]{
                    i * glWidth, startingRow * glHeight,
                    i * glWidth + glWidth, startingRow * glHeight,
                    i * glWidth + glWidth, startingRow * glHeight + glHeight,
                    i * glWidth, startingRow * glHeight + glHeight
            };
            frames.add(Model.createVbo(textureCoordinates));
        }
        return frames;
    }

    public int getTexturePointer() {
        return texturePointer;
    }

    public Renderable getRenderable() {
        return renderable;
    }

    public void setSpriteSheet(Texture spriteSheet) {
        this.spriteSheet = spriteSheet;
    }

    public void setRenderable(Renderable renderable) {
        this.renderable = renderable;
    }

    public void setTexturePointer(int texturePointer) {
        this.texturePointer = texturePointer;
    }

    protected void action(int pointer) {
        spriteSheet.bind(texturePointer);
        renderable.getModel().settId(frames.get(pointer));
    }

    /*protected void action(int texturePointer, int pointer) {
        spriteSheet.bind(texturePointer);
        renderable.getModel().settId((Integer) frames.get(pointer));
    }*/

}