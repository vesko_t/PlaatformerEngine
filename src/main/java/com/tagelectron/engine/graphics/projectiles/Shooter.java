package com.tagelectron.engine.graphics.projectiles;

import com.tagelectron.engine.collision.Collidable;

/**
 * Created by Vesko on 15.11.2018 г..
 */
public interface Shooter {
    void onHit(Collidable collidable);
}
