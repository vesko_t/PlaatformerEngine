package com.tagelectron.engine.graphics.projectiles;

import com.tagelectron.engine.collision.BoundingBox;
import com.tagelectron.engine.collision.Collidable;
import com.tagelectron.engine.entities.Moveable;
import com.tagelectron.engine.entities.Updateable;
import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.rendering.Shader;
import com.tagelectron.engine.world.TexturedTile;
import com.tagelectron.engine.world.Tile;
import com.tagelectron.engine.world.World;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

import java.util.List;

public class Projectile extends Moveable {

    private final String collisionLayerName;
    private Shooter shooter;

    private Vector2f target;

    private float movementConst = 600.0f;
    private boolean updateVelocity = false;
    private BoundingBox boundingBox;

    public Projectile(Projectile projectile) {
        super(projectile.getTexture(), projectile.getPosition(), projectile.shader, projectile.scale, projectile.model);
        collisionLayerName = projectile.collisionLayerName;
        shooter = projectile.shooter;
        if (projectile.target != null) {
            target = new Vector2f(projectile.target);
        }
        boundingBox = new BoundingBox(position, scale);
        movementConst = projectile.movementConst;
        isLightSource = projectile.isLightSource;
    }

    public Projectile(Texture texture, Vector2f position, Shader shader, Vector2f scale, String collisionLayerName, Model model, Shooter shooter) {
        super(texture, position, shader, scale, model);
        this.collisionLayerName = collisionLayerName;
        this.shooter = shooter;
        boundingBox = new BoundingBox(position, scale);
    }

    public Projectile(Texture texture, Vector2f position, Shader shader, Vector2f scale, Model model, String collisionLayerName, Shooter shooter, float movementConst) {
        super(texture, position, shader, scale, model);
        this.collisionLayerName = collisionLayerName;
        this.shooter = shooter;
        this.movementConst = movementConst;
    }

    @Override
    public void update(float v) {
    }

    private void updateCollision() {
        List<Tile> tiles = World.getCurrentMap().getLayers().get(collisionLayerName).getTilesAround(World.getWorldPosition(position), new Vector2i(1));
        for (Tile tile : tiles) {
            if (tile instanceof TexturedTile && boundingBox.isColliding(tile.getBoundingBox())) {
                toBeRemoved = true;
            }
        }
        for (Updateable updateable : Updater.getInView()) {
            if (updateable instanceof Collidable && boundingBox.isColliding(((Collidable) updateable).getEntityBox()) && !updateable.equals(shooter) && !updateable.equals(this)) {
                toBeRemoved = true;
                shooter.onHit((Collidable) updateable);
            }
        }
    }

    @Override
    public void constantUpdate(float v) {
        if (updateVelocity) {
            target.sub(position, velocity);
            velocity.normalize(movementConst * v);
            updateVelocity = false;
        }
        updateCollision();
        updateVelocity();
    }

    @Override
    public void render() {
        super.render();
    }

    public void setTarget(Vector2f target) {
        this.target = target;
        updateVelocity = true;
    }

    public void setRotation(float angle) {
        matrix.setRotationXYZ(0, 0, angle);
        //matrix.scale(new Vector3f(scale, 0));
    }
}
