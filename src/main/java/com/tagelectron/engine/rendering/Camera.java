package com.tagelectron.engine.rendering;

import com.tagelectron.engine.entities.Updateable;
import com.tagelectron.engine.world.Tile;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Created by Veso on 22.1.2018 г..
 */
public class Camera {
    private static Vector2f position;
    private static Matrix4f projection;
    private static int width;
    private static int height;
    private static Vector2f velocity;
    private static Vector2f scale;

    public Camera() {
        this(1280, 720);
    }

    public Camera(int width, int height) {
        this.width = width;
        this.height = height;
        position = new Vector2f(width / -2.0f, height / -2.0f);
        projection = new Matrix4f().ortho2D(-width / 2.0f, width / 2.0f, -height / 2.0f, height / 2.0f);
        velocity = new Vector2f();
        scale = new Vector2f(1);
    }

    public static Vector2f getPosition() {
        return position;
    }

    public static void setPosition(Vector2f position) {
        Camera.position = position;
    }

    public static Matrix4f getProjection() {
        position.add(velocity);
        return projection.scale(new Vector3f(scale, 0)).translate(new Vector3f(position, 0), new Matrix4f());
    }

    public static Matrix4f getUnmovedProjection() {
        return new Matrix4f(projection);
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public static Vector2f getVelocity() {
        return velocity;
    }

    public static void setVelocity(Vector2f velocity) {
        Camera.velocity = velocity;
    }

    /**
     * Checks if an Entity is in view of the camera
     *
     * @param updateable the updateable to check
     * @return is in view
     */
    public static boolean isInView(Updateable updateable) {
        return (updateable.getPosition().x > -position.x - (width / 2)) &&
                (updateable.getPosition().x < -position.x + (width / 2)) &&
                (updateable.getPosition().y > -position.y - height / 2) &&
                (updateable.getPosition().y < -position.y + height / 2);
    }

    public static boolean isInView(Tile tile) {
        return (tile.getPosition().x > -position.x - (width / 2.0f) - tile.getScale().x * 2.0f) &&
                (tile.getPosition().x < -position.x + (width / 2.0f) + tile.getScale().x * 2.0f) &&
                (tile.getPosition().y > -position.y - (height / 2.0f) - tile.getScale().y * 2.0f) &&
                (tile.getPosition().y < -position.y + (height / 2.0f) + tile.getScale().y * 2.0f);
    }

    public static Vector2f getPositionCamera(Vector2f vector2f) {
        return new Vector2f(vector2f.x - (-position.x), vector2f.y - (-position.y));
    }

    public static Vector2f getPositionScreen(Vector2f vector2f) {
        Vector2f cameraPosition = getPositionCamera(vector2f);

        return new Vector2f(cameraPosition.x + width / 2.0f, (cameraPosition.y - height / 2.0f) * -1);
    }

    public static Vector2f getGlPossition(Vector2f vector2f) {

        Vector2f cameraPos = getPositionCamera(vector2f);

        return new Vector2f(cameraPos.x / width * 2, cameraPos.y / height * 2);
    }

    public static void setScale(Vector2f scale) {
        Camera.scale = scale;
    }

    public static Vector2f getScale() {
        return scale;
    }
}
