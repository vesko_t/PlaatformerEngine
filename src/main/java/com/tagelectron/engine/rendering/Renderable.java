package com.tagelectron.engine.rendering;

import com.tagelectron.engine.entities.Updateable;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.Texture;
import com.tagelectron.engine.graphics.animations.Animation;
import com.tagelectron.engine.graphics.animations.MultipleTextureAnimation;
import com.tagelectron.engine.graphics.animations.TextureAnimation;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.*;

/**
 * Created by vesko on 05.02.18.
 */
public abstract class Renderable implements Updateable {

    protected Texture texture;

    protected Matrix4f matrix = new Matrix4f().identity();

    protected Vector2f position;

    protected Shader shader;

    protected Model model;

    protected Animation animation;

    protected Animation secondaryAnimation;

    protected Vector2f scale;

    protected Vector4f color = new Vector4f(1);

    protected boolean toBeRemoved = false;

    protected boolean isLightSource = false;

    public Renderable() {
    }

    public Renderable(Texture texture, Vector2f position, Shader shader, Vector2f scale) {
        this.texture = texture;
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        matrix.translate(new Vector3f(position, 0));
        model = new Model();
    }

    public Renderable(Vector2f position, Shader shader, Vector2f scale) {
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        matrix.translate(new Vector3f(position, 0));
        model = new Model();
    }

    public Renderable(Texture texture, Vector2f position, Shader shader, Vector2f scale, Model model) {
        this.texture = texture;
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        matrix.translate(new Vector3f(position, 0));
        this.model = model;
    }

    public Renderable(Vector2f position, Shader shader, Vector2f scale, Model model) {
        this.position = position;
        this.shader = shader;
        this.scale = scale;
        matrix.translate(new Vector3f(position, 0));
        this.model = model;
    }

    public void render() {
        matrix.setTranslation(new Vector3f(position, 0));
        shader.bind();
        shader.setUniform("color", color);
        if (texture != null && animation == null) {
            shader.setUniform("sampler", 0);
            texture.bind(0);
        } else if (!(animation instanceof MultipleTextureAnimation) && animation != null) {
            shader.setUniform("sampler", 0);
            animation.render();
        } else if (animation != null) {
            for (int i = 0; i < ((MultipleTextureAnimation) animation).getAnimations().size(); i++) {
                TextureAnimation animation = ((MultipleTextureAnimation) this.animation).getAnimations().get(i);
                shader.setUniform("sampler" + (i + 1), animation.getTexturePointer());
            }
            animation.render();
            shader.setUniform("samplers", ((MultipleTextureAnimation) animation).getAnimations().size());
        }
        shader.setUniform("projection", Camera.getProjection());
        shader.setUniform("model", matrix.scale(new Vector3f(scale, 0), new Matrix4f()));
        if (secondaryAnimation != null) {
            secondaryAnimation.render();
        }
        model.render();
        /*if (animation instanceof MultipleTextureAnimation) {
            for (int i = 0; i < GL_MAX_TEXTURE_UNITS; i++) {
                glActiveTexture(GL_TEXTURE0 + i);
                glBindTexture(GL_TEXTURE_2D, 0);
            }
        }*/
    }

    public Texture getTexture() {
        return texture;
    }

    public Matrix4f getMatrix() {
        return matrix;
    }

    public Vector2f getPosition() {
        return position;
    }

    public Shader getShader() {
        return shader;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public Vector2f getScale() {
        return scale;
    }

    public void setScale(Vector2f scale) {
        this.scale = scale;
    }

    public Vector2f leftBottomCorner() {
        return new Vector2f(position.x - scale.x(), position.y - scale.y);
    }

    public Vector2f rightBottomCorner() {
        return new Vector2f(position.x + scale.x(), position.y - scale.y);
    }

    public Vector2f leftTopCorner() {
        return new Vector2f(position.x - scale.x(), position.y + scale.y);
    }

    public Vector2f rightTopCorner() {
        return new Vector2f(position.x + scale.x(), position.y + scale.y);
    }

    public Vector4f getColor() {
        return color;
    }

    public void setColor(Vector4f color) {
        this.color = color;
    }

    @Override
    public boolean isToBeRemoved() {
        return toBeRemoved;
    }

    @Override
    public void setToBeRemoved(boolean toBeRemoved) {
        this.toBeRemoved = toBeRemoved;
    }

    public boolean isLightSource() {
        return isLightSource;
    }

    public void setLightSource(boolean lightSource) {
        isLightSource = lightSource;
    }

}
