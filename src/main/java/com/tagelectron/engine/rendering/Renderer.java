package com.tagelectron.engine.rendering;

import com.tagelectron.engine.entities.Updater;
import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.graphics.Fbo;
import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.graphics.gui.GuiManager;
import com.tagelectron.engine.io.Window;
import com.tagelectron.engine.text.GLText;
import com.tagelectron.engine.text.GLTrueTypeFont;
import com.tagelectron.engine.world.World;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;

public abstract class Renderer implements EventHandler {

    private int FPS;

    private double updateTicks;

    private double deltaTicks = 0;

    private double lastTime = System.nanoTime();

    private int frames = 0;

    private long timer = System.currentTimeMillis();

    private static List<Renderable> renderables = new ArrayList<Renderable>();

    protected World world;

    protected Window window;

    protected boolean showFps = true;

    protected boolean hasBloom = false;

    protected float lastFrames = 60;

    protected static Shader blurShader;
    protected static Shader combineShader;
    private static Fbo lightsFbo;
    private static Fbo blurFbo;
    private static Fbo sceneFbo;
    private static Model model;

    private GLTrueTypeFont font = new GLTrueTypeFont("/fonts/arial.ttf", 32, 1024, 1024);
    private GLText text = new GLText(font, "FPS: 60", new Shader("text"), new Vector3f(1));

    public Renderer(Window window, boolean hasBloom) {
        this.hasBloom = hasBloom;
        this.FPS = 60;
        this.window = window;
        updateTicks = 1000000000 / (double) FPS;
        text.setPosition(new Vector2f(Camera.getWidth() - text.getLength(), 0));
        if (hasBloom) {
            lightsFbo = new Fbo(Window.getWidth(), Window.getHeight());
            blurFbo = new Fbo(Window.getWidth(), Window.getHeight());
            sceneFbo = new Fbo(Window.getWidth(), Window.getHeight());
            model = new Model(new float[]{
                    0, 1,
                    1, 1,
                    1, 0,
                    0, 0,
            });
            setupBloomShaders();
        }
    }

    public Renderer(Window window, int framesPerSecond, boolean hasBloom) {
        this.FPS = framesPerSecond;
        this.window = window;
        this.hasBloom = hasBloom;
        updateTicks = 1000000000 / (double) FPS;
        text.setText("FPS: " + FPS);
        text.setPosition(new Vector2f(Camera.getWidth() - text.getLength(), 0));
        EventManager.getEventHandlers().add(this);
        if (hasBloom) {
            lightsFbo = new Fbo(Window.getWidth(), Window.getHeight());
            blurFbo = new Fbo(Window.getWidth(), Window.getHeight());
            sceneFbo = new Fbo(Window.getWidth(), Window.getHeight());
            model = new Model(new float[]{
                    0, 1,
                    1, 1,
                    1, 0,
                    0, 0,
            });
            setupBloomShaders();
        }
    }

    public Renderer(Window window, World world, boolean hasBloom) {
        this(window, hasBloom);
        this.world = world;
    }

    public Renderer(Window window, World world, int framesPerSecond, boolean hasBloom) {
        this(window, framesPerSecond, hasBloom);
        this.world = world;
    }

    public void run() {
        while (!window.shouldClose()) {
            long now = System.nanoTime();
            deltaTicks += (now - lastTime) / updateTicks;
            lastTime = now;
            if (deltaTicks >= 1) {
                update(lastFrames);
                window.clear();
                befreRender();
                if (hasBloom) {
                    beforeRenderLights();
                    renderLights();
                    afterRenderLights();
                    sceneFbo.bind();
                }
                beforeRenderWorld();
                if (world != null) {
                    world.renderTiles();
                }
                afterRenderWorldBeforeRenderables();
                List<Renderable> toBeRemoved = new ArrayList<>();
                for (int i = 0; i < renderables.size(); i++) {
                    Renderable renderable = renderables.get(i);
                    if (Camera.isInView(renderable)) {
                        renderable.render();
                    }
                    if (renderable.isToBeRemoved()) {
                        toBeRemoved.add(renderable);
                    }
                }
                renderables.removeAll(toBeRemoved);
                if (hasBloom) {
                    sceneFbo.unbind();

                    combineShader.bind();
                    combineShader.setUniform("lightsTexture", 0);
                    combineShader.setUniform("sceneTexture", 1);
                    blurFbo.getTexture().bind(0);
                    sceneFbo.getTexture().bind(1);
                    model.render();
                }
                afterRender();
                if (GuiManager.getGuiElements() != null && GuiManager.getGuiElements().size() > 0) {
                    GuiManager.render();
                }
                if (showFps) {
                    text.render();
                }
                frames++;
                deltaTicks--;
                window.swapBuffers();
                window.prepare();
            }
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                if (showFps) {
                    text.setText("FPS: " + frames);
                } else {
                    System.out.println("FPS : " + frames);
                }
                lastFrames = frames;
                frames = 0;
            }
        }
        window.terminate();
    }

    protected abstract void setupBloomShaders();

    private void renderLights() {
        lightsFbo.bind();
        getRenderables().forEach(renderable -> {
            if (renderable.isLightSource() && Camera.isInView(renderable)) {
                renderable.render();
            }
        });
        lightsFbo.unbind();

        blurFbo.bind();
        blurShader.bind();
        blurShader.setUniform("lightsFboResolution", lightsFbo.getResoulution());
        blurShader.setUniform("lightsTexture", 0);
        lightsFbo.getTexture().bind(0);
        model.bindVertexBuffer();
        model.draw();
        model.unbind();
        blurFbo.unbind();
    }

    protected abstract void update(float deltaTicks);

    public boolean isHasBloom() {
        return hasBloom;
    }

    public void setHasBloom(boolean hasBloom) {
        this.hasBloom = hasBloom;
    }

    public abstract void befreRender();

    public abstract void beforeRenderLights();

    public abstract void afterRenderLights();

    public abstract void beforeRenderWorld();

    public abstract void afterRenderWorldBeforeRenderables();

    public abstract void afterRender();

    public static List<Renderable> getRenderables() {
        return renderables;
    }

    public static void setRenderables(List<Renderable> renderables) {
        Renderer.renderables = renderables;
    }

    public void setShowFps(boolean showFps) {
        this.showFps = showFps;
    }
}
