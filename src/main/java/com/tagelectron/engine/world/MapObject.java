package com.tagelectron.engine.world;

import org.joml.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class MapObject {

    private String name;
    private String type;
    private Vector2f scale;
    private Vector2f position;
    private Map<String, String> properties = new HashMap<>();

    private int id;

    public MapObject(Vector2f position, int id) {
        this.position = position;
        this.id = id;
    }

    public MapObject(String name, Vector2f position, int id) {
        this.name = name;
        this.position = position;
        this.id = id;
    }

    public MapObject(Vector2f position, int id, String type) {
        this.position = position;
        this.id = id;
        this.type = type;
    }

    public MapObject(String name, Vector2f position, int id, String type) {
        this.name = name;
        this.position = position;
        this.id = id;
        this.type = type;
    }

    public MapObject(Vector2f position, int id, Vector2f scale) {
        this.position = position;
        this.id = id;
        this.scale = scale;
    }

    public MapObject(String name, Vector2f position, int id, Vector2f scale) {
        this.name = name;
        this.position = position;
        this.id = id;
        this.scale = scale;
    }

    public MapObject(Vector2f position, int id, String type, Vector2f scale) {
        this.position = position;
        this.id = id;
        this.type = type;
        this.scale = scale;
    }

    public MapObject(String name, Vector2f position, int id, String type, Vector2f scale) {
        this.name = name;
        this.position = position;
        this.id = id;
        this.type = type;
        this.scale = scale;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public Vector2f getScale() {
        return scale;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setScale(Vector2f scale) {
        this.scale = scale;
    }

    public Map<String, String> getProperties() {
        return properties;
    }

}
