package com.tagelectron.engine.world;

import com.tagelectron.engine.io.TileSet;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;
import org.w3c.dom.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Vesko on 9.10.2018 г..
 */
public class TiledMap {

    private int width;
    private int height;
    private Vector2f tileScale;
    private Shader shader;
    private TileSet tileSet;
    private String name;
    private String type;
    private Element mapDocument;
    private Map<String, Layer> layers;
    private Map<String, List<MapObject>> mapObjects;
    private Map<Integer, MapObject> objectList;
    private Map<String, List<MapObject>> mapObjectsTypes;
    private List<String> layerNames = new ArrayList<>();


    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public Vector2f getTileScale() {
        return tileScale;
    }

    public void setTileScale(Vector2f tileScale) {
        this.tileScale = tileScale;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public Map<String, Layer> getLayers() {
        return layers;
    }

    public void setLayers(Map<String, Layer> layers) {
        this.layers = layers;
    }

    public void setTileSet(TileSet tileSet) {
        this.tileSet = tileSet;
    }

    public void render() {
        for (String layer : layerNames) {
            layers.get(layer).render();
        }
    }

    public TileSet getTileSet() {
        return tileSet;
    }

    public Shader getShader() {
        return shader;
    }

    public List<String> getLayerNames() {
        return layerNames;
    }

    public List<MapObject> getObject(String name) {
        return mapObjects.get(name);
    }

    public MapObject getObject(int id) {
        return objectList.get(id);
    }

    public List<MapObject> getObjectByType(String type) {
        return mapObjectsTypes.get(type);
    }

    public void setLayerNames(List<String> layerNames) {
        this.layerNames = layerNames;
    }

    public void setMapObjects(Map<String, List<MapObject>> mapObjects) {
        this.mapObjects = mapObjects;
    }

    public void setObjectList(Map<Integer, MapObject> objectList) {
        this.objectList = objectList;
    }

    public void setMapObjectsTypes(Map<String, List<MapObject>> mapObjectsTypes) {
        this.mapObjectsTypes = mapObjectsTypes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Element getMapDocument() {
        return mapDocument;
    }

    public void setMapDocument(Element mapDocument) {
        this.mapDocument = mapDocument;
    }
}
