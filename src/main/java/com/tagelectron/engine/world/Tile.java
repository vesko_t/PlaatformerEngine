package com.tagelectron.engine.world;

import com.tagelectron.engine.collision.BoundingBox;
import com.tagelectron.engine.rendering.Renderable;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;

/**
 * Created by vesko on 26.01.18.
 */
public class Tile extends Renderable {

    private BoundingBox boundingBox;

    private float movementCost;

    private int heuristicScore;

    private float totalScore;

    private Vector2i wordPosition;

    private Tile parent;

    protected boolean blocked;

    public Tile(int x, int y, Vector2f scale) {
        position = new Vector2f((float) x * scale.x * 2, (float) y * scale.y * 2);
        boundingBox = new BoundingBox(position, scale);
        this.scale = scale;
        wordPosition = new Vector2i(x, y);
        blocked = false;
    }

    public Vector2f getPosition() {
        return position;
    }

    public void setPosition(Vector2f position) {
        this.position = position;
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public Vector2f getScale() {
        return scale;
    }

    public float getMovementCost() {
        return movementCost;
    }

    public void setMovementCost(float movementCost) {
        this.movementCost = movementCost;
    }

    public int getHeuristicScore() {
        return heuristicScore;
    }

    public void setHeuristicScore(int heuristicScore) {
        this.heuristicScore = heuristicScore;
    }

    public float getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(float totalScore) {
        this.totalScore = totalScore;
    }

    public Vector2i getWordPosition() {
        return wordPosition;
    }

    public Tile getParent() {
        return parent;
    }

    public void setParent(Tile parent) {
        this.parent = parent;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    public void setWordPosition(Vector2i wordPosition) {
        this.wordPosition = wordPosition;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void constantUpdate(float delta) {

    }
}
