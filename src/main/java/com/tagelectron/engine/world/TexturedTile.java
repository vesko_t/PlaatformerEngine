package com.tagelectron.engine.world;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;

/**
 * Created by vesko on 29.01.18.
 */
public class TexturedTile extends Tile {

    private Matrix4f matrix = new Matrix4f().identity();

    private int textureBufferId;


    public TexturedTile(int x, int y, Vector2f scale, int textureBufferId) {
        super(x, y, scale);
        matrix.scale(new Vector3f(scale, 0));
        matrix.setTranslation(new Vector3f(position, 0));
        this.textureBufferId = textureBufferId;
        this.blocked = true;
    }



    public Matrix4f getMatrix() {
        return matrix;
    }

    public int getTextureBufferId() {
        return textureBufferId;
    }

}
