package com.tagelectron.engine.world;

import com.tagelectron.engine.graphics.Model;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;
import org.joml.Vector2i;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;

public class Layer {

    private List<List<Tile>> rows = new ArrayList<List<Tile>>();

    private Model model;

    private TiledMap map;

    public Layer(TiledMap map) {
        this.map = map;
        model = new Model();

    }

    public void render() {
        Vector2i cameraPosition = World.getWorldPosition(new Vector2f(-Camera.getPosition().x, -Camera.getPosition().y));
        Shader shader = map.getShader();
        shader.bind();
        shader.setUniform("projection", Camera.getProjection());
        shader.setUniform("sampler", 0);
        map.getTileSet().getTileSet().bind(0);
        model.bindVertexBuffer();
        for (int i = cameraPosition.y - World.getCameraRadius().y - 1; i <= cameraPosition.y + World.getCameraRadius().y + 1; i++) {
            if (i >= 0 && i < rows.size()) {
                List<Tile> row = rows.get(i);
                for (int j = cameraPosition.x - World.getCameraRadius().x - 1; j <= cameraPosition.x + World.getCameraRadius().x + 1; j++) {
                    if (j >= 0 && j < row.size()) {
                        Tile tile = row.get(j);
                        if (tile instanceof TexturedTile) {
                            shader.setUniform("model", tile.getMatrix());
                            shader.setUniform("color", tile.getColor());
                            model.bindTextureBuffer(map.getTileSet().getTextureBuffers().get(((TexturedTile) tile).getTextureBufferId() - 1));
                            model.draw();
                            /*model.settId(map.getTileSet().getTextureBuffers().get(((TexturedTile) tile).getTextureBufferId() - 1));
                            model.render();*/
                        }
                    }
                }
            }
        }
        model.unbind();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public List<Tile> getTilesAround(Vector2i vector2i, Vector2i radius) {
        List<Tile> tiles = new ArrayList<>();
        for (int i = vector2i.y - radius.y; i <= vector2i.y + radius.y; i++) {
            if (i >= 0 && i < rows.size()) {
                for (int j = vector2i.x - radius.x; j <= vector2i.x + radius.x; j++) {
                    if (j >= 0 && j < rows.get(i).size()) {
                        tiles.add(rows.get(i).get(j));
                    }
                }
            }
        }
        return tiles;
    }

    public List<List<Tile>> getRows() {
        return rows;
    }

    public Tile getTile(Vector2i vector2i) {
        return rows.get(vector2i.y).get(vector2i.x);
    }
}
