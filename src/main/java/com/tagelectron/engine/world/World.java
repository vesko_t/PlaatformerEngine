package com.tagelectron.engine.world;

import com.tagelectron.engine.events.EventHandler;
import com.tagelectron.engine.events.EventManager;
import com.tagelectron.engine.io.MapReader;
import com.tagelectron.engine.rendering.Camera;
import com.tagelectron.engine.rendering.Shader;
import org.joml.Vector2f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.w3c.dom.Element;


/**
 * Created by vesko on 26.01.18.
 */
public abstract class World implements EventHandler {

    protected static TiledMap currentMap;

    protected static Vector2i cameraRadius;

    protected static float diagonalTileDistance;

    protected static float horizontalTileDistance;

    protected final Shader mapShader;

    protected static Vector3f scale;

    protected static String mapName;

    protected static TiledMap mapToLoad;

    protected static Element mapElementToLoad;

    public World(Shader shader, Vector3f scale) {
        mapShader = shader;
        this.scale = scale;
        EventManager.getEventHandlers().add(this);
    }

    public World(String nameOfMap, Shader shader, Vector3f scale) {
        World.mapName = nameOfMap;
        mapShader = shader;
        this.scale = scale;
        EventManager.getEventHandlers().add(this);
    }

    protected abstract void afterMapLoad(TiledMap map);

    public static void loadMap(String mapName) {
        World.mapName = mapName;
    }

    public static void loadMap(TiledMap mapToLoad) {
        World.mapToLoad = mapToLoad;
    }

    public TiledMap loadMap(String mapName, Vector3f scale) {
        TiledMap tiledMap = MapReader.loadMap(mapName, scale);
        setWorldVariables(tiledMap);
        return tiledMap;
    }

    public TiledMap loadMap(Element mapElement, Vector3f scale) {
        TiledMap tiledMap = MapReader.loadMap(mapElement, scale);
        setWorldVariables(tiledMap);
        return tiledMap;
    }

    private TiledMap setWorldVariables(TiledMap tiledMap) {
        tiledMap.setShader(mapShader);
        cameraRadius = new Vector2i((int) ((Camera.getWidth() / (tiledMap.getTileScale().x * 2)) / 2), (int) ((Camera.getHeight() / (tiledMap.getTileScale().y * 2)) / 2));
        horizontalTileDistance = tiledMap.getTileScale().x * 2.0f;
        diagonalTileDistance = (float) Math.sqrt(2.0 * Math.pow(horizontalTileDistance, 2));
        return tiledMap;
    }

    public Layer getLayer(String layerName) {
        return currentMap.getLayers().get(layerName);
    }

    public static Vector2i getWorldPosition(Vector2f vector3f) {
        return new Vector2i(Math.round(vector3f.x() / (currentMap.getTileScale().x * 2)), Math.round(vector3f.y() / (currentMap.getTileScale().y * 2)));
    }

    public static Vector2f getFromWorldPosition(Vector2i vector2i) {
        return new Vector2f(vector2i.x * currentMap.getTileScale().x * 2.0f, vector2i.y * currentMap.getTileScale().y * 2.0f);
    }

    public void checkMap() {
        if (mapToLoad != null) {
            currentMap = mapToLoad;
            afterMapLoad(mapToLoad);
            mapToLoad = null;
        }
        if (mapElementToLoad != null) {
            currentMap = loadMap(mapElementToLoad, scale);
            afterMapLoad(currentMap);
            mapElementToLoad = null;
        }

        if (mapName != null) {
            currentMap = loadMap(mapName, scale);
            afterMapLoad(currentMap);
            mapName = null;
        }
    }

    public void renderTiles() {
        checkMap();
        if (currentMap != null) {
            currentMap.render();
        }
    }

    public static Vector2i getCameraRadius() {
        return cameraRadius;
    }

    public static float getDiagonalTileDistance() {
        return diagonalTileDistance;
    }

    public static float getHorizontalTileDistance() {
        return horizontalTileDistance;
    }

    public static TiledMap getCurrentMap() {
        return currentMap;
    }

    public static void setCurrentMap(TiledMap currentMap) {
        World.currentMap = currentMap;
    }

    public static Vector3f getScale() {
        return scale;
    }
}
