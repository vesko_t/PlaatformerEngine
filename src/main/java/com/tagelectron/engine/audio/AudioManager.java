package com.tagelectron.engine.audio;

import com.tagelectron.engine.io.FileLoader;
import org.joml.Vector2f;
import org.lwjgl.openal.*;
import org.lwjgl.system.MemoryStack;

import java.io.IOException;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.openal.AL10.*;
import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.stb.STBVorbis.stb_vorbis_decode_memory;

/**
 * Created by Vesko on 1.11.2018 г..
 */
public class AudioManager {

    private static long device;
    private static long context;
    private static List<Integer> buffers = new ArrayList<>();
    private static boolean initialized = false;

    public AudioManager() {
        String deviceName = alcGetString(0, ALC_DEFAULT_DEVICE_SPECIFIER);
        device = alcOpenDevice(deviceName);

        int[] attributes = new int[1];

        context = alcCreateContext(device, attributes);
        alcMakeContextCurrent(context);

        ALCCapabilities alcCapabilities = ALC.createCapabilities(device);
        ALCapabilities alCapabilities = AL.createCapabilities(alcCapabilities);
        initialized = true;
    }

    public static int loadSound(String fileName) {
        ShortBuffer rawAudioBuffer = null;

        int channels;
        int sampleRate;

        try (MemoryStack stack = MemoryStack.stackPush()) {
            IntBuffer channelsBuffer = stack.mallocInt(1);
            IntBuffer sampleRateBuffer = stack.mallocInt(1);

            try {
                rawAudioBuffer = stb_vorbis_decode_memory(FileLoader.loadResurce(fileName), channelsBuffer, sampleRateBuffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            channels = channelsBuffer.get(0);
            sampleRate = sampleRateBuffer.get(0);
        }
        int format = -1;
        if (channels == 1) {
            format = AL_FORMAT_MONO16;
        } else if (channels == 2) {
            format = AL_FORMAT_STEREO16;
        }

        int bufferPointer = alGenBuffers();

        alBufferData(bufferPointer, format, rawAudioBuffer, sampleRate);
        buffers.add(bufferPointer);
        return bufferPointer;
    }

    public static void setListenerPosition(Vector2f position) {
        alListener3f(AL_POSITION, position.x, position.y, 0);
    }

    public static void setListenerVelocity(Vector2f velocity) {
        alListener3f(AL_VELOCITY, velocity.x, velocity.y, 0);
    }

    public void cleanup() {
        buffers.forEach(AL10::alDeleteBuffers);
        alcMakeContextCurrent(0);
        alcDestroyContext(context);
        alcCloseDevice(device);
    }

    public static boolean isInitialized() {
        return initialized;
    }
}
