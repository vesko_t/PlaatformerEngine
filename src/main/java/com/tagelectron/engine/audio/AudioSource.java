package com.tagelectron.engine.audio;

import org.joml.Vector2f;

import static org.lwjgl.openal.AL10.*;

/**
 * Created by Vesko on 1.11.2018 г..
 */
public class AudioSource {

    int sourceId;

    private float gain = 1;
    private float pitch = 1;

    public AudioSource() {
        sourceId = alGenSources();
    }

    public AudioSource(Vector2f position, Vector2f velocity) {
        sourceId = alGenSources();
        setPosition(position);
        setVelocity(velocity);
    }

    public void setLooping(boolean looping) {
        alSourcei(sourceId, AL_LOOPING, looping ? AL_TRUE : AL_FALSE);
    }

    public boolean isPlaying() {
        return alGetSourcei(sourceId, AL_SOURCE_STATE) == AL_PLAYING;
    }

    public void stop() {
        alSourceStop(sourceId);
    }

    public void start(int buffer) {
        alSourcei(sourceId, AL_BUFFER, buffer);
        alSourcePlay(sourceId);
    }

    public void start() {
        alSourcePlay(sourceId);
    }

    public void pause() {
        alSourcePause(sourceId);
    }

    public void setPosition(Vector2f position) {
        alSource3f(sourceId, AL_POSITION, position.x, position.y, 0);
    }

    public void setVelocity(Vector2f velocity) {
        alSource3f(sourceId, AL_VELOCITY, velocity.x, velocity.y, 0);
    }

    public void setGain(float gain) {
        this.gain = gain;
        alSourcef(sourceId, AL_GAIN, gain);
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
        alSourcef(sourceId, AL_PITCH, pitch);
    }
}