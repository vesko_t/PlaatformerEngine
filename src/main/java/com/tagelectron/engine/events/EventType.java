package com.tagelectron.engine.events;

/**
 * Created by Vesko on 22.11.2018 г..
 */
public enum EventType {
    PlayerDeath, EscapePressed, WorldLoading, WorldLoaded, Save, QuickLoad, MapChange, LoadNewMap, LoadLastSave, OpenContainer;
    private String command;

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }
}
