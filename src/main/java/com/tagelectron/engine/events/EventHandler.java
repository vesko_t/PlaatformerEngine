package com.tagelectron.engine.events;

/**
 * Created by Vesko on 22.11.2018 г..
 */
public interface EventHandler {
    public void onEvent(EventType eventType);
}
