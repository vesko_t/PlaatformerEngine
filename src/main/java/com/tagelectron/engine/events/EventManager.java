package com.tagelectron.engine.events;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vesko on 22.11.2018 г..
 */
public class EventManager {

    private static List<EventHandler> eventHandlers = new ArrayList<>();

    public static void sendEvent(EventType eventType) {
        for (int i = 0; i < eventHandlers.size(); i++) {
            eventHandlers.get(i).onEvent(eventType);
        }
    }


    public static List<EventHandler> getEventHandlers() {
        return eventHandlers;
    }
}
